/*
 * Copyright 2023-2024 Shiide
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Pixie {

using Crosscut;

[GtkTemplate(ui = "/io/shiide/Pixie/window.ui")]
public class Window : Adw.ApplicationWindow {

  Service.PathProvider pathProvider;
  Service.DirectoryLister directoryLister;
  Service.PixbufCache pixbufCache;
  ViewScreen.Controller viewScreenCtl;

  [GtkChild] unowned Gtk.Picture img_current;
  [GtkChild] unowned Gtk.Spinner loading_spinner;
  [GtkChild] unowned Gtk.Button btn_open_folder;
  [GtkChild] unowned Gtk.Button btn_step_back;
  [GtkChild] unowned Gtk.Button btn_step_forward;
  [GtkChild] unowned Gtk.Label lbl_navigation_info;
  [GtkChild] unowned Gtk.Label lbl_current_path;
  [GtkChild] unowned Gtk.Label lbl_current_filename;
  [GtkChild] unowned Gtk.Label lbl_current_size;

  public Window(Gtk.Application app) {
    Object(application: app);
    createServices();
    createControllers();
    connectSignals();
    setStartupState();
  }

  construct {
    ActionEntry[] action_entries = {
      { "sort_files_by_name", this.onSortFilesByName },
      { "sort_files_by_creation_date", this.onSortFilesByCreationDate },
      { "sort_files_by_modification_date", this.onSortFilesByModificationDate },
      { "sort_files_ascending", this.onSortFilesAscending },
      { "sort_files_descending", this.onSortFilesDescending },
    };
    this.add_action_entries(action_entries, this);
  }

  void setStartupState() {
    setImageListToFolder(pathProvider.getDefaultPath());
  }

  void createServices() {
    pathProvider = new Service.PathProvider();
    directoryLister = new Service.DirectoryLister(new Model.ImageTypeFilter());
    pixbufCache = new Service.PixbufCache();
  }

  void createControllers() {
    viewScreenCtl = new ViewScreen.Controller(
      directoryLister,
      pixbufCache,
      img_current
    );
  }

  void connectSignals() {
    btn_open_folder.clicked.connect(openFolderAction);
    btn_step_back.clicked.connect(browseStepBackAction);
    btn_step_forward.clicked.connect(browseStepForwardAction);
    
    viewScreenCtl.currentFolderChanged.connect(onCurrentFolderChanged);
    viewScreenCtl.currentImageChanged.connect(onCurrentImageChanged);
    viewScreenCtl.imageLoadingBegun.connect(onImageLoadingBegun);
    viewScreenCtl.imageLoadingEnded.connect(onImageLoadingEnded);

    var keyEventCtl = new Gtk.EventControllerKey();
    keyEventCtl.key_pressed.connect(onKeyPressed);
    ((Gtk.Widget)this).add_controller(keyEventCtl);
  }

  void setImageListToFolder(string folderPath) {
    viewScreenCtl.setFolderPath(folderPath);
  }

  void quitAction() {
    close();
  }

  void browseStepBackAction() {
    viewScreenCtl.stepBack();
  }

  void browseStepForwardAction() {
    viewScreenCtl.stepForward();
  }

  void onCurrentFolderChanged(string folderPath, uint maxImages) {
    lbl_current_path.set_label(folderPath);

    bool haveImages = (maxImages > 0);
    btn_step_back.set_sensitive(haveImages);
    btn_step_forward.set_sensitive(haveImages);
    lbl_current_size.set_visible(haveImages);
    lbl_current_filename.set_visible(haveImages);
  }

  void onCurrentImageChanged(uint imageIndex, string fileName) {
    lbl_current_filename.set_label(@"$(fileName)");
    lbl_navigation_info.set_label(
        createNavigationInfo(imageIndex, viewScreenCtl.getImageCount()));
  }

  string createNavigationInfo(uint currentImage, uint maxImages) {
    return (maxImages > 0
        ? @"$(currentImage) / $(maxImages)"
        : "No images");
  }

  void openFolderAction() {
    // TODO FileChooserDialog is deprecated
    var chooserDialog = new Gtk.FileChooserDialog(
        "Open Directory", this, Gtk.FileChooserAction.SELECT_FOLDER,
        "Cancel", Gtk.ResponseType.CANCEL, "Open", Gtk.ResponseType.ACCEPT);
    chooserDialog.response.connect_after((response) => {
      if (response == Gtk.ResponseType.ACCEPT && chooserDialog.get_current_folder() != null) {
        string folderPath = chooserDialog.get_current_folder().get_path();
        setImageListToFolder(folderPath);
      }
      chooserDialog.destroy();
    });
    chooserDialog.show();
  }
  
  void onImageLoadingBegun() {
    loading_spinner.spinning = true;
  }

  void onImageLoadingEnded(uint width, uint height) {
    GLib.Idle.add(() => {
      loading_spinner.spinning = false;
      lbl_current_size.set_label(@"$(width) × $(height)");
      return false;
    });
  }

  bool onKeyPressed(Gtk.EventControllerKey _, uint keyval, uint keycode, Gdk.ModifierType state) {
    switch (keyval) {
      case Gdk.Key.Escape:
        quitAction();
        break;
      case Gdk.Key.Left:
        browseStepBackAction();
        break;
      case Gdk.Key.Right:
        browseStepForwardAction();
        break;
      case Gdk.Key.Home:
        viewScreenCtl.jumpToFront();
        break;
      case Gdk.Key.End:
        viewScreenCtl.jumpToBack();
        break;
    }
    return true;
  }

  void onSortFilesByName() {
    viewScreenCtl.setFilelistSortType(Pixie.Model.ListingSortType.BY_NAME);
  }
  void onSortFilesByCreationDate() {
    viewScreenCtl.setFilelistSortType(Pixie.Model.ListingSortType.BY_CREATION_DATE);
  }
  void onSortFilesByModificationDate() {
    viewScreenCtl.setFilelistSortType(Pixie.Model.ListingSortType.BY_MODIFICATION_DATE);
  }
  void onSortFilesAscending() {
    viewScreenCtl.setFilelistSortDirection(Pixie.Model.ListingSortDirection.ASCENDING);
  }
  void onSortFilesDescending() {
    viewScreenCtl.setFilelistSortDirection(Pixie.Model.ListingSortDirection.DESCENDING);
  }
}

}

