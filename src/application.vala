/*
 * Copyright 2023-2024 Shiide
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Pixie {

using Crosscut;

public class Application : Adw.Application {
  
  public Application() {
    Object(
        application_id: "io.shiide.Pixie",
        flags: ApplicationFlags.HANDLES_OPEN
    );
  }

  construct {
    ActionEntry[] action_entries = {
      { "about", this.on_about_action },
      { "preferences", this.on_preferences_action },
      { "quit", this.quit }
    };
    this.add_action_entries(action_entries, this);
    this.set_accels_for_action("app.quit", {"<primary>q"});
  }

  public override void activate() {
    base.activate();
    var win = this.active_window;
    if (win == null) {
      win = new Pixie.Window(this);
    }
    win.present();
  }

  public override void open(File[] files, string hint) {
    foreach (File file in files) {
      Logging.debug(@"App.open: should open '$(file.get_path())'");
    }
  }

  private void on_preferences_action() {
    // --
  }

  private void on_about_action() {
    string[] developers = { "Shiide" };
    new Adw.AboutWindow() {
      transient_for = this.active_window,
      application_name = "Pixie",
      application_icon = "io.shiide.Pixie",
      developer_name = "Shiide",
      version = "0.1.0",
      developers = developers,
      copyright = "© 2023-2024 Shiide",
    }.present();
  }
}
  
}
