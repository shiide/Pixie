/*
 * Copyright 2023-2024 Shiide
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

using Pixie.Crosscut;

namespace Pixie.Service {

public class DirectoryLister {

  Pixie.Model.FileTypeFilter fileTypeFilter;

  public DirectoryLister(Pixie.Model.FileTypeFilter fileTypeFilter) {
    this.fileTypeFilter = fileTypeFilter;
  }

  public Array<Pixie.Model.FileEntry> list(string path) {
    var fileEntries = new Array<Pixie.Model.FileEntry>();
    try {
      Logging.debug(@"== Directory '$path'");
      var directory = File.new_for_path(path);
      var enumerator = directory.enumerate_children(
          "*", FileQueryInfoFlags.NOFOLLOW_SYMLINKS, null);
      FileInfo fileInfo;
      while ((fileInfo = enumerator.next_file()) != null) {
        if (fileTypeFilter.match(fileInfo)) {
          Logging.debug(@"Adding '$(fileInfo.get_name())'");
          var newEntry = Pixie.Model.FileEntry() {
            systemName = fileInfo.get_name(),
            displayName = fileInfo.get_display_name(),
            creationDate = fileInfo.get_creation_date_time(),
            modificationDate = fileInfo.get_modification_date_time(),
          };
          fileEntries.append_val(newEntry);
        } else {
          Logging.debug(@"Skipping '$(fileInfo.get_name())'");
        }
      }
    } catch (Error err) {
      Logging.error(@"DirectoryLister.list: $(err.message)");
    }

    return fileEntries;
  }
}

}

