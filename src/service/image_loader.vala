/*
 * Copyright 2023-2024 Shiide
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Pixie.Service {

using Crosscut;

public class ImageLoaderWorker : Object {

  string filePath;
  Cancellable cancellable;

  public signal void done(string filePath, Gdk.Pixbuf pixbuf);
  public signal void error(string filePath, string message);

  public ImageLoaderWorker(string filePath, Cancellable cancellable) {
    this.filePath = filePath;
    this.cancellable = cancellable;
  }

  public void load() {
    try {
      var pixbuf = new Gdk.Pixbuf.from_file(filePath);
      if (! cancellable.is_cancelled()) {
        done(filePath, pixbuf);
      }
    } catch (Error err) {
      error(filePath, err.message);
    }
  }
}

}

