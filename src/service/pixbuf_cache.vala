/*
 * Copyright 2023-2024 Shiide
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Pixie.Service {

public class PixbufCache {

  const uint MAX_ITEMS = 20;

  Gee.HashMap<string, Gdk.Pixbuf> store = new Gee.HashMap<string, Gdk.Pixbuf>();
  Array<string> paths = new Array<string>();

  public bool has(string path) {
    return store.has_key(path);
  }

  public Gdk.Pixbuf get(string path) {
    return (store.has_key(path)
        ? store[path]
        : null);
  }

  public void setGuarded(string path, Gdk.Pixbuf pixbuf) {
    if (! store.has_key(path)) {
      set(path, pixbuf);
    }
  }

  public void set(string path, Gdk.Pixbuf pixbuf) {
    store.set(path, pixbuf);
    paths.append_val(path);
    if (paths.length > MAX_ITEMS) {
      var pathToRemove = paths.index(0);
      store.unset(pathToRemove);
      paths.remove_index(0);
    }
  }
}

}
