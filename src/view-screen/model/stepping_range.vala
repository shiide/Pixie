/*
 * Copyright 2023-2024 Shiide
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Pixie.ViewScreen.Model {

public class SteppingRange : Object {

  uint current;
  uint length;

  public signal void stepped(uint current);

  public uint getCurrent() {
    return current;
  }

  public void resetWithLength(uint length) {
    this.length = length;
    current = 0;
    stepped(current);
  }

  public void stepForwardBy(uint places)
      requires(places > 0)
  {
    if (length > 1) {
      current = (current == length - 1
          ? 0
          : (current + places < length
              ? current + places
              : length - 1));
      stepped(current);
    }
  }

  public void stepBackBy(uint places)
      requires(places > 0)
  {
    if (length > 1) {
      current = (current == 0
          ? length - 1
          : (places <= current + 1
              ? current - places
              : 0));
      stepped(current);
    }
  }

  public void jumpToFront() {
    if (current != 0) {
      current = 0;
      stepped(current);
    }
  }

  public void jumpToFrontAndNotify() {
    current = 0;
    stepped(current);
  }

  public void jumpToBack() {
    if (length > 0 && current != length - 1) {
      current = length - 1;
      stepped(current);
    }
  }
}

}
