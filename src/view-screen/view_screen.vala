/*
 * Copyright 2023-2024 Shiide
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Pixie.ViewScreen {

using Crosscut;

public class Controller : Object {

  Model.SteppingRange steppingRange;
  Pixie.Model.FolderListing folderListing;
  Pixie.Service.DirectoryLister directoryLister;
  Pixie.Service.PixbufCache pixbufCache;
  Cancellable loaderCancellable = null;
  unowned Gtk.Picture pictureWidget;

  public signal void currentFolderChanged(string folderPath, uint maxImages);
  public signal void currentImageChanged(uint imageIndex, string fileName);
  public signal void imageLoadingBegun();
  public signal void imageLoadingEnded(uint width, uint height);

  public Controller(
      Pixie.Service.DirectoryLister directoryLister,
      Pixie.Service.PixbufCache pixbufCache,
      Gtk.Picture pictureWidget
  ) {
    steppingRange = new Model.SteppingRange();
    folderListing = new Pixie.Model.FolderListing();
    this.directoryLister = directoryLister;
    this.pixbufCache = pixbufCache;
    this.pictureWidget = pictureWidget;

    connectSignals();
  }

  void connectSignals() {
    steppingRange.stepped.connect(onCurrentImageChanged);

    var scrollEventCtl = new Gtk.EventControllerScroll(
        Gtk.EventControllerScrollFlags.VERTICAL);
    scrollEventCtl.scroll.connect(onPictureWidgetScroll);
    ((Gtk.Widget)pictureWidget).add_controller(scrollEventCtl);
  }

  public void setFolderPath(string folderPath) {
    folderListing.setEntryList(folderPath, directoryLister.list(folderPath));
    steppingRange.resetWithLength(folderListing.length());
    pictureWidget.set_opacity(folderListing.length() > 0 ? 1.0 : 0.0);

    currentFolderChanged(folderPath, folderListing.length());
    if (folderListing.length() < 1) {
      currentImageChanged(0, "");
    }
  }

  public uint getImageCount() {
    return folderListing.length();
  }

  public void stepForward() {
    steppingRange.stepForwardBy(1);
  }

  public void stepBack() {
    steppingRange.stepBackBy(1);
  }

  public void jumpToFront() {
    steppingRange.jumpToFront();
  }

  public void jumpToBack() {
    steppingRange.jumpToBack();
  }

  public void setFilelistSortType(Pixie.Model.ListingSortType sortType) {
    folderListing.setSortType(sortType);
    steppingRange.jumpToFrontAndNotify();
  }

  public void setFilelistSortDirection(Pixie.Model.ListingSortDirection sortDirection) {
    folderListing.setSortDirection(sortDirection);
    steppingRange.jumpToFrontAndNotify();
  }

  uint currentDisplayIndex() {
    return steppingRange.getCurrent() + 1;
  }
  
  void onCurrentImageChanged(Object _, uint imageIndex) {
    if (folderListing.length() > 0) {

      loaderCancellable?.cancel();

      var imagePath = folderListing.getFullPath(imageIndex);
      if (pixbufCache.has(imagePath)) {
        currentImageChanged(currentDisplayIndex(), folderListing.getDisplayName(imageIndex));
        onImageLoaded(imagePath, pixbufCache.get(imagePath));
        return;
      }

      loaderCancellable = new Cancellable();
      var imageWorker = new Pixie.Service.ImageLoaderWorker(imagePath, loaderCancellable);
      imageWorker.done.connect(onImageLoaded);
      imageWorker.error.connect(onImageLoaderError);

      imageLoadingBegun();
      pictureWidget.set_opacity(0.5);
      currentImageChanged(
        currentDisplayIndex(),
        folderListing.getDisplayName(steppingRange.getCurrent()));
      new Thread<void>("image_loader", imageWorker.load);
    }
  }

  void onImageLoaded(string filePath, Gdk.Pixbuf pixbuf) {
    pixbufCache.setGuarded(filePath, pixbuf);
    GLib.Idle.add(() => {
      pictureWidget.set_pixbuf(pixbuf);
      pictureWidget.set_opacity(1.0);
      return false;
    });
    imageLoadingEnded(pixbuf.width, pixbuf.height);
  }

  void onImageLoaderError(string filePath, string message) {
    Logging.debug(@"Error loading image '$(filePath)': $(message)");
  }

  bool onPictureWidgetScroll(Gtk.EventControllerScroll _, double __, double delta) {
    if (delta == -1) {
      stepBack();
    } else if (delta == 1) {
      stepForward();
    }
    return true;
  }
}

}

