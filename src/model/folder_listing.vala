/*
 * Copyright 2023-2024 Shiide
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Pixie.Model {

using Crosscut;

public enum ListingSortType {
  BY_NAME,
  BY_CREATION_DATE,
  BY_MODIFICATION_DATE,
}

public enum ListingSortDirection {
  ASCENDING,
  DESCENDING,
}

public class FolderListing {
  string rootPath;
  Array<FileEntry> fileEntries;
  ListingSortType currentSortType = ListingSortType.BY_MODIFICATION_DATE;
  ListingSortDirection currentSortDirection = ListingSortDirection.DESCENDING;

  public void setEntryList(
      string rootPath,
      Array<FileEntry> fileEntries
  ) {
    this.rootPath = rootPath;
    this.fileEntries = fileEntries;
    applySorting();
  }

  public uint length() {
    return fileEntries.length;
  }

  public string getPath() {
    return rootPath;
  }

  public string getDisplayName(uint index) {
    return fileEntries.index(index).displayName;
  }

  public string getFullPath(uint index) {
    return Path.build_filename(rootPath, fileEntries.index(index).systemName);
  }

  public void setSorting(ListingSortType sortType, ListingSortDirection sortDirection) {
    currentSortType = sortType;
    currentSortDirection = sortDirection;
    applySorting();
  }

  public void setSortType(ListingSortType sortType) {
    currentSortType = sortType;
    applySorting();
  }

  public void setSortDirection(ListingSortDirection sortDirection) {
    currentSortDirection = sortDirection;
    applySorting();
  }

  public void flipSortDirection() {
    currentSortDirection = ((int)currentSortDirection == ListingSortDirection.ASCENDING
        ? ListingSortDirection.DESCENDING
        : ListingSortDirection.ASCENDING);
    applySorting();
  }

  void applySorting() {
    fileEntries.sort(getCompareFunc());
  }

  CompareFunc<Pixie.Model.FileEntry?> getCompareFunc() {
    switch ((int)currentSortType) {
      case ListingSortType.BY_NAME:
        return ((int)currentSortDirection == ListingSortDirection.ASCENDING
            ? compareEntryNameAsc : compareEntryNameDesc);
      case ListingSortType.BY_CREATION_DATE:
        return ((int)currentSortDirection == ListingSortDirection.ASCENDING
          ? compareCreationDateAsc : compareCreationDateDesc);
      case ListingSortType.BY_MODIFICATION_DATE:
        return ((int)currentSortDirection == ListingSortDirection.ASCENDING
          ? compareModificationDateAsc : compareModificationDateDesc);
      default:
        return compareModificationDateDesc;
    }
  }

  GLib.CompareFunc<Pixie.Model.FileEntry?> compareEntryNameAsc = (fileA, fileB) => {
    return GLib.strcmp(fileA.systemName, fileB.systemName);
  };
  GLib.CompareFunc<Pixie.Model.FileEntry?> compareEntryNameDesc = (fileA, fileB) => {
    return GLib.strcmp(fileB.systemName, fileA.systemName);
  };
  GLib.CompareFunc<Pixie.Model.FileEntry?> compareCreationDateAsc = (fileA, fileB) => {
    return fileA.creationDate.compare(fileB.creationDate);
  };
  GLib.CompareFunc<Pixie.Model.FileEntry?> compareCreationDateDesc = (fileA, fileB) => {
    return fileB.creationDate.compare(fileA.creationDate);
  };
  GLib.CompareFunc<Pixie.Model.FileEntry?> compareModificationDateAsc = (fileA, fileB) => {
    return fileA.modificationDate.compare(fileB.modificationDate);
  };
  GLib.CompareFunc<Pixie.Model.FileEntry?> compareModificationDateDesc = (fileA, fileB) => {
    return fileB.modificationDate.compare(fileA.modificationDate);
  };
}

}
