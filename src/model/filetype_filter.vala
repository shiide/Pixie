/*
 * Copyright 2023-2024 Shiide
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Pixie.Model {

public interface FileTypeFilter : Object {

  public abstract bool match(FileInfo fileInfo);
}

public class ImageTypeFilter : Object, FileTypeFilter {

  Gee.Set<string> supportedExtensions = new Gee.HashSet<string>();

  public ImageTypeFilter() {
    Object();
    foreach (var format in Gdk.Pixbuf.get_formats()) {
      supportedExtensions.add_all_array(format.get_extensions());
    }
  }

  public bool match(FileInfo fileInfo) {
    return (fileInfo.get_file_type() != FileType.DIRECTORY
        && !fileInfo.get_is_symlink()
        && isExtensionSupported(fileInfo.get_name()));
  }

  bool isExtensionSupported(string fileName) {
    return supportedExtensions.contains(extractExtension(fileName));
  }

  string extractExtension(string fileName) {
    int index = fileName.last_index_of_char('.');
    return (index <= 0
        ? ""
        : fileName[index+1:].down());
  }
}

}
